#!/bin/bash
source /etc/environment
lock_file="/etc/sensu/git.lock"
if [ -f $lock_file ]; then
    exit 0
else
    cd /etc/sensu/
    touch $lock_file
    need_restart=false
    chmod 0600 $HOME/.ssh/id_rsa
    ssh-keyscan -t rsa,dsa github.com > $HOME/.ssh/known_hosts
    if (( `git fetch sensu  2>&1 | grep -v "known hosts" | wc -l` != 0 )); then
        git reset --hard
        #git checkout sensu/SENSU_REPO_BRANCH -- conf.d
        git checkout sensu/SENSU_REPO_BRANCH -- extensions
        git checkout sensu/SENSU_REPO_BRANCH -- handlers
        git checkout sensu/SENSU_REPO_BRANCH -- mutators
        git checkout sensu/SENSU_REPO_BRANCH -- plugins
        #pkill sensu-client
        #/opt/sensu/bin/sensu-client -c /etc/sensu/config.json -d /etc/sensu/conf.d -e /etc/sensu/extensions
    fi
    rm $lock_file
fi