#!/bin/bash

export RABBITMQ_HOST=${RABBITMQ_HOST:-sensu-rabbitmq}
export RABBITMQ_PORT=${RABBITMQ_PORT:-5671}
export RABBITMQ_PASSWORD=${RABBITMQ_PASSWORD:-sensurabbitmqpassword}
export CLIENT_NAME=${CLIENT_NAME:-$HOSTNAME}
export CLIENT_IP_ADDRESS=${CLIENT_IP_ADDRESS:-127.0.0.1}
export SUBSCRIBE=${SUBSCRIBE:-127.0.0.1}
export SENSU_REPO=${SENSU_REPO:-git@github.com:Sensu/sensu.git}
export SENSU_REPO_BRANCH=${SENSU_REPO_BRANCH:-master}

sed -i  "s/RABBITMQ_HOST/${RABBITMQ_HOST}/g" /etc/sensu/config.json
sed -i  "s/RABBITMQ_PORT/${RABBITMQ_PORT}/g" /etc/sensu/config.json
sed -i  "s/RABBITMQ_PASSWORD/${RABBITMQ_PASSWORD}/g" /etc/sensu/config.json
sed -i  "s/CLIENT_NAME/${CLIENT_NAME}/g" /etc/sensu/conf.d/client.json
sed -i  "s/CLIENT_IP_ADDRESS/${CLIENT_IP_ADDRESS}/g" /etc/sensu/conf.d/client.json
sed -i  "s/\"SUBSCRIBE\"/\"${SUBSCRIBE}\", \"${CLIENT_NAME}\"/g" /etc/sensu/conf.d/client.json
sed -i  "s|SENSU_REPO_BRANCH|${SENSU_REPO_BRANCH}|g" /config/gitclient.sh

cd /etc/sensu/
git init
git init
git remote add sensu ${SENSU_REPO}

echo "* * * * * /config/gitclient.sh > /var/log/gitclient.log 2>&1" > /config/crontab_backup.txt
crontab /config/crontab_backup.txt

/usr/bin/supervisord